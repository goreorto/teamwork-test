package customerimporter

import (
	"fmt"
	"reflect"
	"testing"
)

func TestCustomerTagBasedParse(t *testing.T) {
	testcases := []struct {
		casename            string
		customerLine        string
		csvMapping          map[string]int
		expectedErr         error
		expectedFirstName   string
		expectedEmail       string
		expectedEmailDomain string
	}{
		{
			casename:            "Happy",
			customerLine:        "john@doe.com,john,doe",
			csvMapping:          map[string]int{"email": 0, "first_name": 1, "lastname": 2},
			expectedErr:         nil,
			expectedFirstName:   "john",
			expectedEmail:       "john@doe.com",
			expectedEmailDomain: "doe.com",
		},
		{
			casename:            "EmailError",
			customerLine:        "john doe.com,john,doe",
			csvMapping:          map[string]int{"email": 0, "first_name": 1, "lastname": 2},
			expectedErr:         &EmailError{"john doe.com", fmt.Errorf("invalid email")},
			expectedFirstName:   "john",
			expectedEmail:       "john doe.com",
			expectedEmailDomain: "",
		},
		{
			casename:            "CustomerError",
			customerLine:        "john@doe.com",
			csvMapping:          map[string]int{"email": 0, "first_name": 1, "lastname": 2},
			expectedErr:         &CustomerError{"john@doe.com", fmt.Errorf("invalid customer")},
			expectedFirstName:   "",
			expectedEmail:       "",
			expectedEmailDomain: "",
		},
	}

	for _, testcase := range testcases {
		customer := &Customer{}
		actualErr := customer.TagBasedParse(testcase.customerLine, testcase.csvMapping)
		if reflect.TypeOf(actualErr) != reflect.TypeOf(testcase.expectedErr) {
			t.Errorf("Test %s: expected %q, got %q", testcase.casename, reflect.TypeOf(testcase.expectedErr), reflect.TypeOf(actualErr))
		}
		if customer.Email != testcase.expectedEmail {
			t.Errorf("Test %s: expected %v, got %v", testcase.casename, testcase.expectedEmail, customer.Email)
		}
		if customer.EmailDomain != testcase.expectedEmailDomain {
			t.Errorf("Test %s: expected %v, got %v", testcase.casename, testcase.expectedEmailDomain, customer.EmailDomain)
		}
		if customer.FirstName != testcase.expectedFirstName {
			t.Errorf("Test %s: expected %v, got %v", testcase.casename, testcase.expectedFirstName, customer.FirstName)
		}
	}
}

func TestCustomerParse(t *testing.T) {
	testcases := []struct {
		casename            string
		customerLine        string
		csvMapping          map[string]int
		expectedErr         error
		expectedFirstName   string
		expectedEmail       string
		expectedEmailDomain string
	}{
		{
			casename:            "Happy",
			customerLine:        "john@doe.com,john,doe",
			csvMapping:          map[string]int{"email": 0, "first_name": 1, "lastname": 2},
			expectedErr:         nil,
			expectedFirstName:   "",
			expectedEmail:       "john@doe.com",
			expectedEmailDomain: "doe.com",
		},
		{
			casename:            "EmailError",
			customerLine:        "john doe.com,john,doe",
			csvMapping:          map[string]int{"email": 0, "first_name": 1, "lastname": 2},
			expectedErr:         &EmailError{"john doe.com", fmt.Errorf("invalid email")},
			expectedFirstName:   "",
			expectedEmail:       "john doe.com",
			expectedEmailDomain: "",
		},
		{
			casename:            "CustomerError",
			customerLine:        "john@doe.com",
			csvMapping:          map[string]int{"email": 0, "first_name": 1, "lastname": 2},
			expectedErr:         &CustomerError{"john@doe.com", fmt.Errorf("invalid customer")},
			expectedFirstName:   "",
			expectedEmail:       "",
			expectedEmailDomain: "",
		},
	}

	for _, testcase := range testcases {
		customer := &Customer{}
		actualErr := customer.Parse(testcase.customerLine, testcase.csvMapping)
		if reflect.TypeOf(actualErr) != reflect.TypeOf(testcase.expectedErr) {
			t.Errorf("Test %s: expected %q, got %q", testcase.casename, reflect.TypeOf(testcase.expectedErr), reflect.TypeOf(actualErr))
		}
		if customer.Email != testcase.expectedEmail {
			t.Errorf("Test %s: expected %v, got %v", testcase.casename, testcase.expectedEmail, customer.Email)
		}
		if customer.EmailDomain != testcase.expectedEmailDomain {
			t.Errorf("Test %s: expected %v, got %v", testcase.casename, testcase.expectedEmailDomain, customer.EmailDomain)
		}
		if customer.FirstName != testcase.expectedFirstName {
			t.Errorf("Test %s: expected %v, got %v", testcase.casename, testcase.expectedFirstName, customer.FirstName)
		}
	}
}

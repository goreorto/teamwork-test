package customerimporter

import (
	"fmt"
	"reflect"
	"strings"
)

// Customer is a customer
type Customer struct {
	FirstName   string `header:"first_name"`
	Email       string `header:"email"`
	EmailDomain string
}

var customerHeaderMapping = func(i interface{}) map[string]int {
	t := reflect.TypeOf(i)
	positions := map[string]int{}

	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		if tag := field.Tag.Get("header"); tag != "" {
			positions[tag] = i
		}
	}

	return positions
}(Customer{})

// TagBasedParse fills up the customer based on Customer tags
func (c *Customer) TagBasedParse(customerLine string, csvMapping map[string]int) error {
	customerAttr := strings.Split(customerLine, ",")
	if len(customerAttr) < len(csvMapping) {
		return NewCustomerError(customerLine, fmt.Errorf("invalid customer"))
	}

	v := reflect.ValueOf(c).Elem()
	for header, pos := range customerHeaderMapping {
		v.Field(pos).SetString(customerAttr[csvMapping[header]])
	}

	emailParts := strings.Split(c.Email, "@")
	if len(emailParts) < 2 {
		return NewEmailError(c.Email, fmt.Errorf("invalid email"))
	}

	c.EmailDomain = emailParts[1]

	return nil
}

// Parse inspects
func (c *Customer) Parse(customerLine string, csvMapping map[string]int) error {
	customerAttr := strings.Split(customerLine, ",")
	if len(customerAttr) < len(csvMapping) {
		return NewCustomerError(customerLine, fmt.Errorf("invalid customer"))
	}

	c.Email = customerAttr[csvMapping["email"]]

	emailParts := strings.Split(c.Email, "@")
	if len(emailParts) < 2 {
		return NewEmailError(c.Email, fmt.Errorf("invalid email"))
	}

	c.EmailDomain = emailParts[1]

	return nil
}

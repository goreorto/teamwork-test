package customerimporter

import (
	"fmt"
)

type (
	// ReadFileError records CSV file error
	ReadFileError struct {
		filePath string
		Err      error
	}
	// CustomerError is produced when CSV headers doesn't match CSV row
	CustomerError struct {
		customerString string
		Err            error
	}
	// EmailError is produced when CSV customer email doesn't container at least 2 element when split by @
	EmailError struct {
		email string
		Err   error
	}
	// HeaderNotFoundError is produced when CSV given header is not found
	HeaderNotFoundError struct {
		header string
	}
)

func (e *ReadFileError) Error() string { return fmt.Sprintf("`%s`: %s", e.filePath, e.Err.Error()) }
func (e *CustomerError) Error() string {
	return fmt.Sprintf("`%s`: %s", e.customerString, e.Err.Error())
}
func (e *EmailError) Error() string          { return fmt.Sprintf("`%s`: %s", e.email, e.Err.Error()) }
func (e *HeaderNotFoundError) Error() string { return fmt.Sprintf("`%s`: header not found", e.header) }

// NewReadFileError returns, as an error, a new ReadFileError
// with the given file path and error details.
// As a convenience, if err is nil, NewReadFileError returns nil.
func NewReadFileError(filePath string, err error) error {
	if err == nil {
		return nil
	}
	return &ReadFileError{filePath, err}
}

// NewCustomerError returns, as an error, a new CustomerError
// with the given customer string and error details.
// As a convenience, if err is nil, NewCustomerError returns nil.
func NewCustomerError(customerString string, err error) error {
	if err == nil {
		return nil
	}
	return &CustomerError{customerString, err}
}

// NewEmailError returns, as an error, a new EmailError
// with the given email and error details.
// As a convenience, if err is nil, NewEmailError returns nil.
func NewEmailError(email string, err error) error {
	if err == nil {
		return nil
	}
	return &EmailError{email, err}
}

// NewHeaderNotFound returns, as an error, a new HeaderFoundError
// with the given header name.
func NewHeaderNotFound(header string) error {
	return &HeaderNotFoundError{header}
}

// IsReadFileError returns a boolean indicating whether the error is known to report that the CSV could not be read
func IsReadFileError(e error) bool {
	_, ok := e.(*ReadFileError)
	return ok
}

// IsCustomerError returns a boolean indicating whether the error is known to report that the customer is invalid
func IsCustomerError(e error) bool {
	_, ok := e.(*CustomerError)
	return ok
}

// IsEmailError returns a boolean indicating whether the error is known to report that the customer email is invalid
func IsEmailError(e error) bool {
	_, ok := e.(*EmailError)
	return ok
}

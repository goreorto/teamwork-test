// Package customerimporter reads from the given customers.csv file and returns a
// sorted (data structure of your choice) of email domains along with the number
// of customers with e-mail addresses for each domain.  Any errors should be
// logged (or handled). Performance matters (this is only ~3k lines, but *could*
// be 1m lines or run on a small machine).
package customerimporter

import (
	"bufio"
	"io"
	//"log"
	"strings"
)

// Domains maps the user count for each email domain given a CSV file
type Domains map[string]int

// Add adds a new increments domain count or adds a new entry to Domains
func (d Domains) Add(domain string) {
	d[domain] = d[domain] + 1
}

func (d *Domains) Read(file io.Reader, cancelOnError bool) error {
	scanner := bufio.NewScanner(file)

	scanner.Scan()
	csvMapping := map[string]int{}
	for i, header := range strings.Split(scanner.Text(), ",") {
		csvMapping[header] = i
	}

	if _, ok := csvMapping["email"]; !ok {
		return NewHeaderNotFound("email")
	}

	for scanner.Scan() {
		customer := &Customer{}

		if err := customer.Parse(scanner.Text(), csvMapping); err != nil {
			if !cancelOnError {
				//log.Println(err)
				continue
			}

			return err
		}

		d.Add(customer.EmailDomain)
	}

	return NewReadFileError("REPLACE", scanner.Err())
}

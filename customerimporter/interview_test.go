package customerimporter

import (
	"bytes"
	"io"
	"reflect"
	"testing"
)

// Based on https://golang.org/src/bufio/scan_test.go#L387
type endlessZeros struct {
	first bool
}

func (e *endlessZeros) Read(p []byte) (int, error) {
	if e.first {
		e.first = false
		s := "email,first_name\n"
		n := copy(p, ([]byte(s)))
		return n, nil
	}

	return 0, nil
}

func TestDomainsRead(t *testing.T) {
	testcases := []struct {
		casename        string
		reader          io.Reader
		cancelOnError   bool
		expectedErr     error
		expectedDomains map[string]int
	}{
		{
			casename:        "Happy",
			reader:          bytes.NewBufferString("email,first_name\na@gmail.com,a\nb@gmail.com,b\nc@live.com,c\nd@live.com,d\ne@live.com,e\n"),
			cancelOnError:   true,
			expectedErr:     nil,
			expectedDomains: map[string]int{"gmail.com": 2, "live.com": 3},
		},
		{
			casename:        "Cancel on no email header",
			reader:          bytes.NewBufferString("first_name\na@gmail.com,a\nb@gmail.com,b\nc@live.com,c\nd@live.com,d\ne@live.com,e\n"),
			cancelOnError:   true,
			expectedErr:     &HeaderNotFoundError{},
			expectedDomains: map[string]int{},
		},
		{
			casename:        "Cancel on customer parsing error",
			reader:          bytes.NewBufferString("email,first_name\na.com,a\nb@gmail.com,b\nc@live.com,c\nd@live.com,d\ne@live.com,e\n"),
			cancelOnError:   true,
			expectedErr:     &EmailError{},
			expectedDomains: map[string]int{},
		},
		{
			casename:        "Scanner error",
			reader:          &endlessZeros{true},
			cancelOnError:   true,
			expectedErr:     &ReadFileError{},
			expectedDomains: map[string]int{},
		},
		{
			casename:        "Continue on error",
			reader:          bytes.NewBufferString("email,first_name\na.com,a\nb@gmail.com,b\nc@live.com,c\nd@live.com,d\ne@live.com,e\n"),
			cancelOnError:   false,
			expectedErr:     nil,
			expectedDomains: map[string]int{"gmail.com": 1, "live.com": 3},
		},
	}

	for _, testcase := range testcases {
		subject := Domains{}
		actualErr := subject.Read(testcase.reader, testcase.cancelOnError)

		if reflect.TypeOf(actualErr) != reflect.TypeOf(testcase.expectedErr) {
			t.Errorf("Test %s: expected %q, got %q", testcase.casename, reflect.TypeOf(testcase.expectedErr), reflect.TypeOf(actualErr))
		}

		if len(testcase.expectedDomains) != len(subject) {
			t.Errorf("Test %s: expected domains len %d, got %d", testcase.casename, len(testcase.expectedDomains), len(subject))
		}

		for k, v := range testcase.expectedDomains {
			if subject[k] != v {
				t.Errorf("Test %s: expected %s = %d, got %d", testcase.casename, k, v, subject[k])
			}
		}
	}
}

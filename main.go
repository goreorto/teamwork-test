package main

import (
	"log"
	"os"
	"time"

	"teamwork.com/egotestical/customerimporter"
)

func main() {
	now := time.Now()
	file, err := os.Open("customers.csv")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	domains := customerimporter.Domains{}
	err = domains.Read(file, false)

	if err != nil {
		log.Fatal(err)
	}

	log.Println(time.Since(now))
	//log.Printf("domains: %#v", domains)
}
